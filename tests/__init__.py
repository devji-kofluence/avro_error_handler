
from avro_error_handler import sanitize_object
import json

schema_file_path = 'testschema.avsc'
with open(schema_file_path, 'r') as s:
  dict_schema = json.loads(s.read())

obj_missing_obj = {
    "orderId": "1",
    "total": "10,000",
    "customer": "Devji"
}

obj_missing_attr = {
    "orderId": "1",
    "total": "10,000",
    "customer": "Devji",
    "productInner": {
        "productId": "P1001"
    }
}

obj_good = {
    "orderId": "1",
    "total": "10,000",
    "customer": "Devji",
    "productInner": {
        "productId": "P1001",
        "productName": "Parle-G"
    }
}

# simple replacements with type
type_defaults = {
    'string': 'NotAvailable',
    'int': 0,
}

# complex object example
productInner = {
    "productId": "P1002",
    "productName": "Monacco"
    }

object_defaults = {
    'kofluence.order.productInner': productInner
}

# run tests  
namespace = 'kofluence.order'

# test 1: all good
sanitized_object = sanitize_object (
    dict_schema, 
    obj_good,
    namespace, 
    type_defaults,
    object_defaults)

if 'corrections' in sanitized_object and len (sanitized_object ['corrections']) > 0:
    print ('test: good object: fail')
    print (sanitized_object)
else:
    print ('test: good object: pass')


# test 2: missing attribute
sanitized_object = sanitize_object (
    dict_schema, 
    obj_missing_attr,
    namespace, 
    type_defaults,
    object_defaults)

if len (sanitized_object['corrections']) != 1:
    print ('test: missing attr: fail')
    print (sanitized_object)
else:
    if sanitized_object ['corrections'][0]['value_set'] == 'NotAvailable':
        print ('test: missing attr: pass')
    else:
        print ('test: missing attr: fail as value does not match')


# test 2: missing object
sanitized_object = sanitize_object (
    dict_schema, 
    obj_missing_obj,
    namespace, 
    type_defaults,
    object_defaults)

if len (sanitized_object['corrections']) != 1:
    print ('test: missing obj: fail')
    print (sanitized_object)
else:
    if sanitized_object ['corrections'][0]['value_set'] == productInner:
        print ('test: missing obj: pass')
    else:
        print ('test: missing obj: fail: value doesnt match')
