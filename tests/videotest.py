
from avro_error_handler import sanitize_object
import json
import logging
logging.basicConfig(level=logging.DEBUG)


schema_file_path = './schemas/com.kofluence.yt.VideoDetails.avsc'
with open(schema_file_path, 'r') as s:
  dict_schema = json.loads(s.read())

obj_missing_obj = {
    "orderId": "1",
    "total": "10,000",
    "customer": "Devji"
}

# test video
video_obj = {
   "etag":"aufdyKPVESHf0Me7JQW-kN7jp3E",
   "id":"XVLIda6Pn9U",
   "snippet":{
      "publishedAt":"2021-09-22T09:23:04Z",
      "channelId":"UCJ-od_N9MA-ccpMZ9ICc9Vg",
      "title":"New ringhthon 2021,love ringhthon,Best ringhthon",
      "description":"",
      "thumbnails":{
         "default":{
            "url":"https://i.ytimg.com/vi/XVLIda6Pn9U/default.jpg",
            "height":90
         },
         "medium":{
            "url":"https://i.ytimg.com/vi/XVLIda6Pn9U/mqdefault.jpg",
            "width":320,
            "height":180
         },
         "high":{
            "url":"https://i.ytimg.com/vi/XVLIda6Pn9U/hqdefault.jpg",
            "width":480,
            "height":360
         },
         "standard":{
            "url":"https://i.ytimg.com/vi/XVLIda6Pn9U/sddefault.jpg",
            "width":640,
            "height":480
         }
      },
      "channelTitle":"sk ringhthon",
      "categoryId":"22",
      "liveBroadcastContent":"none"
   },
   "contentDetails":{
      "duration":"PT33S",
      "dimension":"2d",
      "definition":"sd",
      "caption":"false",
      "licensedContent":False,
      "contentRating":{
         
      },
      "projection":"rectangular"
   },
   "status":{
      "uploadStatus":"processed",
      "privacyStatus":"public",
      "license":"youtube",
      "embeddable":True,
      "publicStatsViewable":True,
      "madeForKids":False
   },
   "statistics":{
      "viewCount":"21",
      "likeCount":"5",
      "favoriteCount":"0",
      "commentCount":"0"
   },
   "topicDetails":{
      "topicCategories":[
         "https://en.wikipedia.org/wiki/Music",
         "https://en.wikipedia.org/wiki/Music_of_Asia"
      ]
   }
}

maxres = {
    'url': 'test',
    'height': 100,
    'width': 100
}
tags = ['tag1', 'tag2']
localized = {
   "title":"New ringhthon 2021,love ringhthon,Best ringhthon",
   "description":""
}
metadata = {
   "generatorId": "test",
   "jobId": "test",
   "createdAt": "test",
   "messageId": "test",
   "isAuth": False,
}

# test object missing:
# - com.kofluence.yt.VideoDetails.kind   
# - com.kofluence.yt.VideoDetails.snippet.thumbnails.default.width
# - com.kofluence.yt.VideoDetails.snippet.thumbnails.maxres
# - com.kofluence.yt.VideoDetails.snippet.tags
# - com.kofluence.yt.VideoDetails.snippet.defaultAudioLanguage
# - com.kofluence.yt.VideoDetails.snippet.localized

type_defaults = {
   'int': 0,
   'string': 'Not Available'
}
object_defaults = {
    'com.kofluence.yt.VideoDetails.snippet.thumbnails.maxres': maxres,
    'com.kofluence.yt.VideoDetails.snippet.tags': tags,
    'com.kofluence.yt.VideoDetails.snippet.defaultAudioLanguage': 'English',
    'com.kofluence.yt.VideoDetails.snippet.localized': localized,
    'com.kofluence.yt.VideoDetails.metaData': metadata
}
namespace = 'com.kofluence.yt.VideoDetails'
try:
    sanitized_object = sanitize_object (
        dict_schema, 
        video_obj,
        namespace, 
        type_defaults,
        object_defaults)
    print ('---------------------------------')
    print (sanitized_object)
except ValueError as e:
    error_data = e.args[0]
    print ('error message')
    print (error_data ['error'])
    print ('object')
    print (error_data ['object'])
