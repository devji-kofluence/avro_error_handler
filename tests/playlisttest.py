
from avro_error_handler import sanitize_object
import json
import logging
logging.basicConfig(level=logging.DEBUG)


schema_file_path = './schemas/com.kofluence.yt.PlaylistItem.avsc'
with open(schema_file_path, 'r') as s:
  dict_schema = json.loads(s.read())

# test channel
channel_obj = {}

metadata = {
   "generatorId": "test",
   "jobId": "test",
   "createdAt": "test",
   "messageId": "test",
   "isAuth": False,
}

type_defaults = {
   'int': 0,
   'string': 'Not Available'
}
object_defaults = {
}
namespace = 'com.kofluence.yt.PlaylistItem'
try:
    sanitized_object = sanitize_object (
        dict_schema, 
        channel_obj,
        namespace, 
        type_defaults,
        object_defaults)
    print ('---------------------------------')
    print (sanitized_object)
except ValueError as e:
    error_data = e.args[0]
    print ('error message')
    print (error_data ['error'])
    print ('object')
    print (error_data ['object'])
