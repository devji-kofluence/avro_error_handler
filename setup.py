# -*- coding: utf-8 -*-

from setuptools import setup, find_packages  # type: ignore

# read the contents of your README file
from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='avro_error_handler',
    version='0.0.6',
    author='Devji Chhanga',
    author_email='devji.chhanga@kofluence.com',
    description='Detect and correct errors in JSON objects against an Avro schema',
    long_description=long_description,
    long_description_content_type='text/markdown',
    keywords="json avro error correction",
    python_requires='>= 3.7',
    packages=find_packages(exclude=("tests",)),  # type: ignore
    url="https://kofluence.com",
    classifiers=[
        "Programming Language :: Python :: 3.7"
    ],
    install_requires=[
        'fastavro'
    ]
)