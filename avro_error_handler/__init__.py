from fastavro.validation import validate
from fastavro._validate_common import ValidationError
import logging

def extract_field_name (s):
    """
        extract field name from fastavro 
        validation error text
    """
    return s[s.find("(")+1:s.find(")")]

def extract_actual_value (s):
    """
        extract actual value present for a 
        field from fastavro validation error text
    """
    return s.split(" is ")[1].split(" ")[0]

def extract_expected_type (s):
    """
        extract expected type for a field 
        from fastavro validation error text
    """
    if "fields" in s:
        test = extract_field_name(s)
        return extract_field_name(s)
    return s.split(" expected ")[1].split("\"")[0]

def get_default_value (key, default_values):
    """
        get default value for a data type
        from given dict of defaults
    """
    try:
        return default_values [key]
    except KeyError:
        return None

def put(d, keys, item):
    if "." in keys:
        key, rest = keys.split(".", 1)
        if key not in d:
            d[key] = {}
        put(d[key], rest, item)
    else:
        d[keys] = item
    return d


def deep_access(obj, keylist, value_to_set):
    """
        doctstring
    """
    val = obj
    for key in keylist:
        try:
         val = val[key]
        except KeyError:
         val [key] = value_to_set
    return obj

def find_errors (schema, object):
    """
        doctstring
    """
    try:
        validate (object, schema, raise_errors=True)
        return True
    except ValidationError as myExc:
        string = str (myExc)
        errors = {}
        for item in string.split("\n"):
            if item == "[" or item == "]":
                continue
            errors ['field'] = extract_field_name (item)
            errors ['found'] = extract_actual_value (item)
            type_str = extract_expected_type (item)
            errors ['expected'] = extract_expected_type (item)
        return errors

def sanitize_object (schema, object, namespace, type_defaults, object_defaults):
    """
        doctstring
    """
    outcome = find_errors (schema, object)
    corrections = []
    for i in range (0, 50):
        object ['corrections'] = corrections
        if outcome is True:
            logging.debug ('returned after {} iterations'.format (i))
            return object
        # treat None, can add more branches for other errors
        if outcome ['found'] == 'None':
            if outcome ['field'] in object_defaults:
                deep_access_location = outcome['field'].replace(namespace, '')
                keys = deep_access_location[1:]
                def_value = get_default_value (outcome['field'],  object_defaults)
                object = put (object, keys, def_value)
                correction = {
                    'error': outcome,
                    'value_set': def_value
                }
                corrections.append (correction)
            elif outcome ['expected'] in type_defaults:
                deep_access_location = outcome['field'].replace(namespace, '')
                keys = deep_access_location[1:]
                def_value = get_default_value (outcome['expected'],  type_defaults)
                if def_value is None:
                    def_value = get_default_value (outcome['field'],  object_defaults)
                object = put (object, keys, def_value)
                correction = {
                    'error': outcome,
                    'value_set': def_value
                }
                corrections.append (correction)
            else:
                error_object = {
                    'error': 'default value not found for {}'.format(outcome ['expected']), 
                    'object': object
                }
                raise ValueError (error_object)
        # keep looping
        outcome = find_errors (schema, object)
    return object
