Match and correct missing values in a JSON object against an Avro schema

# Installation
```
sudo python setup.py install
```
DEV NOTE: RUN THIS EVERYTIME YOU PULL

# Tests
```
cd tests
python __init__.py 
```

# Inputs
1. schema: schema object to validate against
2. object: object to correct
3. namespace: object's namespace string
4. type_defaults: a dict containing default values for data types
5. object_defaults: a dict containing default values for objects

# Output object
Entire object with inserted defaults is returned with following extra fields:
1. corrections: a list of errors and corrections made
```
[
    {
        'error': {
            'field': string,
            'found': string,
            'expected': string,
        },
        'value_set': string / object
    }
]
```

# Errors
Throws ```ValueError`` when input does not contain missing values

# Reference
Please refer to tests